ENVIRONMENT = "development"
puts "environment: #{ENVIRONMENT}"

SETTINGS = {
  :hostname => "dese.moodlemed",
  :ip => "192.168.12.12",
  :memory => 2048,
  :cpus => 1,
  :disks => [
    { :path => "disks/u01.vmdk", :size => 10, :format => "VMDK" }
  ],
  :scripts => [
    { :path => "scripts/disks_add.sh", :args => [1] },
    {
      :path => "scripts/environment_config.sh",
      :args => ["dese.moodlemed.dev", ENVIRONMENT]
    }
  ]
}
