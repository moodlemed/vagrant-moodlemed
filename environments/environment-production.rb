ENVIRONMENT = "production"
puts "environment: #{ENVIRONMENT}"

SETTINGS = {
  :hostname => "moodlemed",
  :ip => "192.168.11.11",
  :memory => 2048,
  :cpus => 1,
  :disks => [
    { :path => "disks/u01.vmdk", :size => 30, :format => "VMDK"},
    { :path => "disks/u02.vmdk", :size => 30, :format => "VMDK"}
  ],
  :scripts => [
    { :path => "scripts/disks_add.sh", :args => [2] },
    {
      :path => "scripts/environment_config.sh",
      :args => ["www.moodlemed.dev", ENVIRONMENT]
    }
  ]
}
