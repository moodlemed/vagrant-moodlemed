.PHONY: up clean development production

up: | environment.rb
	vagrant up

up.log: | environment.rb
	vagrant up > up.log 2>&1

development production:
	cp --force environments/environment-$@.rb environment.rb

clean:
	vagrant destroy --force
	rm --force environment.rb up.log

environment.rb:
	$(error Use "make development" or "make production" first)
