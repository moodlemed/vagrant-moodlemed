#!/bin/bash
#######################################
# Instala e configura o NGINX e o PHP-FPM
# Globals:
#   None
# Arguments:
#   Nome do usuário do NGINX e do PHP-FPM; "nginx" (padrão)
# Returns:
#   None
#######################################

readonly USERNAME="${1:-nginx}"

yum -y install nginx php-fpm
# http://nginx.org/en/docs/dirindex.html
# http://nginx.org/en/docs/varindex.html
# https://github.com/h5bp/server-configs-nginx/
# https://www.nginx.com/resources/wiki/start/topics/tutorials/config_pitfalls/#virtualbox
tee /etc/nginx/nginx.conf <<END
user ${USERNAME};
worker_processes auto;
worker_rlimit_nofile 8192;
events {
  worker_connections 8000;
  use epoll;
  multi_accept on;
}
error_log /var/log/nginx/error.log;
pid /var/run/nginx.pid;
http {
  server_tokens off;
  include /etc/nginx/mime.types;
  default_type application/octet-stream;
  access_log /var/log/nginx/access.log;
  keepalive_timeout 20;
END
# https://docs.vagrantup.com/v2/synced-folders/virtualbox.html
# http://www.dmo.ca/blog/detecting-virtualization-on-linux/
if [[ "$(dmidecode -s system-product-name)" = "VirtualBox" ]]; then
  tee -a /etc/nginx/nginx.conf <<END
  sendfile off;
END
else
  tee -a /etc/nginx/nginx.conf <<END
  sendfile on;
END
fi
tee -a /etc/nginx/nginx.conf <<END
  tcp_nopush on;
  gzip on;
  gzip_comp_level 9;
  gzip_min_length 256;
  gzip_proxied any;
  gzip_vary on;
  gzip_types
    application/atom+xml
    application/javascript
    application/json
    application/rss+xml
    application/vnd.ms-fontobject
    application/xhtml+xml
    text/xml
    image/svg+xml
    image/x-icon
    text/css
    text/plain
    text/x-component;
  gzip_disable "msie6";
  add_header X-Frame-Options SAMEORIGIN;
  add_header X-Content-Type-Options nosniff;
  add_header X-XSS-Protection "1; mode=block";
  add_header "X-UA-Compatible" "IE=Edge";
  types_hash_max_size 1024;
  include /etc/nginx/conf.d/*.conf;
}
END
# https://secure.php.net/manual/en/install.fpm.configuration.php
# https://docs.moodle.org/29/en/Nginx#PHP-FPM
tee /etc/php-fpm.d/www.conf <<END
[www]
listen = /var/run/php-fpm/php-fpm.sock
listen.allowed_clients = 127.0.0.1
user = ${USERNAME}
pm = static
pm.max_children = 64
slowlog = /var/log/php-fpm/${USERNAME}-slow.log
php_admin_value[error_log] = /var/log/php-fpm/${USERNAME}-error.log
php_admin_flag[log_errors] = on
php_value[session.save_handler] = files
php_value[session.save_path] = /var/lib/php/session
security.limit_extensions = .php
END
systemctl enable nginx.service
systemctl start nginx.service
systemctl enable php-fpm.service
systemctl start php-fpm.service
