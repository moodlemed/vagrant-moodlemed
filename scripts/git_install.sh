#!/bin/bash
#######################################
# Instala e configura o Git
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################

# https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server
yum -y install git
useradd git
su git -c "mkdir /home/git/.ssh"
su git -c "chmod 0700 /home/git/.ssh"
su git -c "touch /home/git/.ssh/authorized_keys"
su git -c "chmod 0600 /home/git/.ssh/authorized_keys"
echo "/usr/bin/git-shell" >> /etc/shells
chsh -s /usr/bin/git-shell git

su git -c "git clone --bare -b MOODLE_29_STABLE --single-branch git://git.moodle.org/moodle.git wwwroot.git"
