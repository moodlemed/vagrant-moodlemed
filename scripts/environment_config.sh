#!/bin/bash
#######################################
# Configura o ambiente
# Globals:
#   None
# Arguments:
#   Domínio
#   Nome do ambiente; "development" (padrão) ou "production"
# Returns:
#   None
#######################################

if [[ "$#" -lt 1 ]]; then
  echo "Usage: $0 server_name [environment]" >&2
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

readonly SERVER_NAME="$1"
readonly ENVIRONMENT="${2:-development}"

cwd=.
virtualbox=false
if [[ "$(dmidecode -s system-product-name)" = "VirtualBox" ]]; then
  cwd=/vagrant
  virtualbox=true
fi

sudo bash "${cwd}/scripts/before.sh"
if [[ "${virtualbox}" = true ]]; then
  sudo bash "${cwd}/scripts/iptables_config.sh" \
    5432 \
    443 \
    80
else
  sudo bash "${cwd}/scripts/firewalld_config.sh" \
    http \
    https \
    postgresql
fi
sudo bash "${cwd}/scripts/php_install.sh" \
  "${ENVIRONMENT}"
sudo bash "${cwd}/scripts/nginx_install.sh" \
  nginx
sudo bash "${cwd}/scripts/postgres_install.sh" \
  /srv/u01/pgdata \
  /srv/u01/xlogdir \
  postgres \
  "${ENVIRONMENT}"
sudo bash "${cwd}/scripts/memcached_install.sh"
sudo bash "${cwd}/scripts/git_install.sh"
sudo bash "${cwd}/scripts/moodle_install.sh" \
  /srv/u01/moodlemed/wwwroot \
  /srv/u02/moodlemed/dataroot \
  "${SERVER_NAME}" \
  moodlemed \
  MoodleMed \
  "moodlemed@${SERVER_NAME}" \
  /srv/u01/pgdata \
  80 \
  443 \
  nginx \
  postgres \
  "${ENVIRONMENT}"
if [[ "${ENVIRONMENT}" = "development" ]]; then
  sudo bash "${cwd}/scripts/users_add.sh" \
    /srv/u01/moodlemed/wwwroot \
    /srv/u02/moodlemed/dataroot \
    "${ENVIRONMENT}" \
    postgres
fi
sudo bash "${cwd}/scripts/mv_idxspc.sh" \
  moodlemed \
  moodlemed_idxspc \
  postgres
