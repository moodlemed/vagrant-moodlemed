#!/bin/bash
#######################################
# Instala e configura o Memcached
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################

yum --assumeyes install memcached php-pecl-memcached
sed --in-place 's,CACHESIZE="64",CACHESIZE="512",' /etc/sysconfig/memcached
systemctl enable memcached.service
systemctl start memcached.service
systemctl restart php-fpm.service
