#!/bin/bash
#######################################
# Particiona, formata e configura os discos
# Globals:
#   None
# Arguments:
#   Número de discos
# Returns:
#   None
#######################################

if [[ "$#" -lt 1 ]]; then
  echo "Usage: $0 disks" >&2
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

for i in $(seq -w 01 "${1:-1}"); do
  l="$(printf "%b" "\\$(printf "%o" "$((i + 97))")")"
  sudo parted -s "/dev/sd${l}" mklabel msdos mkpart primary 0% 100%
  sudo mkfs.xfs "/dev/sd${l}1"
  sudo xfs_admin -L "/srv/u${i}" "/dev/sd${l}1"
  sudo mkdir -p "/srv/u${i}"
  echo "LABEL=/srv/u${i} /srv/u${i} xfs defaults 0 0" | tee -a /etc/fstab
  sudo mount "/srv/u${i}"
done
