#!/bin/bash
#######################################
# Configura o iptables
# Globals:
#   None
# Arguments:
#   Portas (variádico)
# Returns:
#   None
#######################################

for port in "$@"; do
  iptables --insert INPUT --protocol tcp --destination-port "${port}" --jump ACCEPT
done
iptables-save
