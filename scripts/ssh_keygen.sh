#!/bin/bash
#######################################
# Cria as chaves SSH, as comprime e adiciona a chave SSH pública ao Git
# Globals:
#   None
# Arguments:
#   Nome do usuário
# Returns:
#   None
#######################################

cwd=.
if [[ "$(dmidecode -s system-product-name)" = "VirtualBox" ]]; then
  cwd=/vagrant
fi

while IFS=, read -r login; do
  mkdir -p "/home/${login}/.ssh"
  ssh-keygen -b 4096 -t rsa -N "" -C "$1" -f "/home/$1/.ssh/id_rsa"
  tar -czf "/home/$1/ssh_keys.tar.gz" -C "/home/$1/.ssh" id_rsa id_rsa.pub
  cat "/home/$1/.ssh/id_rsa.pub" >> /home/git/.ssh/authorized_keys
done < "${cwd}/users.csv"
