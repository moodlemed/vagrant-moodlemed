#!/bin/bash
#######################################
# Instala e configura o PostgreSQL
# Globals:
#   None
# Arguments:
#   Caminho do diretório "pgdata"
#   Caminho do diretório "xlogdir"
#   Nome do usuário do PostgreSQL; "postgres" (padrão)
#   Nome do ambiente; "development" (padrão) ou "production"
# Returns:
#   None
#######################################

if [[ "$#" -lt 2 ]]; then
  echo "Uso: $0 pgdata xlogdir [postgres] [environment]" >&2
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

readonly PGDATA="$1"
readonly XLOGDIR="$2"
readonly USERNAME="${3:-postgres}"
readonly ENVIRONMENT="${5:-development}"

yum --assumeyes install http://yum.postgresql.org/9.2/redhat/rhel-7-x86_64/pgdg-centos92-9.2-1.noarch.rpm
yum --assumeyes install pgbouncer pgtune python-psycopg2 postgresql postgresql-contrib postgresql-server php-pgsql
# http://www.postgresql.org/docs/9.2/static/app-initdb.html
mkdir --parents "${PGDATA}" "${XLOGDIR}"
chown "${USERNAME}:${USERNAME}" --recursive "${PGDATA}" "${XLOGDIR}"
# /usr/share/doc/postgresql-9.2.xx/README.rpm-dist
semanage fcontext --add --type postgresql_db_t "${PGDATA}(/.*)?"
restorecon -R "${PGDATA}"
PGSETUP_INITDB_OPTIONS="--pgdata=${PGDATA} --encoding=UTF8 --locale=pt_BR.UTF8 --xlogdir=${XLOGDIR}" postgresql-setup initdb
# http://www.postgresql.org/docs/9.2/static/kernel-resources.html
# http://leopard.in.ua/2013/09/05/postgresql-sessting-shared-memory/
__phys_pages="$(getconf _PHYS_PAGES)"
_shmall="$((__phys_pages / 2))"
echo "kernel.shmall=${_shmall}" >> /etc/sysctl.conf
_page_size="$(getconf PAGE_SIZE)"
_shmmax="$((_shmall * _page_size))"
echo "kernel.shmmax=${_shmmax}" >> /etc/sysctl.conf
# http://www.postgresql.org/docs/9.2/static/auth-pg-hba-conf.html
tee "${PGDATA}/pg_hba.conf" <<END
local all ${USERNAME} ident map=${ENVIRONMENT}
host all all 127.0.0.1/32 md5
host all all ::1/128 md5
END
# http://www.postgresql.org/docs/9.2/static/auth-username-maps.html
tee "${PGDATA}/pg_ident.conf" <<END
${ENVIRONMENT} ${USERNAME} ${USERNAME}
END
su --shell=/bin/bash "${USERNAME}" --command="mv ${PGDATA}/postgresql.conf ${PGDATA}/postgresql.conf.orig"
su --shell=/bin/bash "${USERNAME}" --command="pgtune --input-config ${PGDATA}/postgresql.conf.orig --output-config ${PGDATA}/postgresql.conf --memory $(($(grep MemTotal /proc/meminfo | awk '{print $2}') * 1024)) --type Web --connections 150"
# /usr/share/doc/postgresql-9.2.xx/README.rpm-dist
tee /etc/systemd/system/postgresql.service <<END
.include /lib/systemd/system/postgresql.service
[Service]
Environment=PGDATA=${PGDATA}
END
systemctl daemon-reload
systemctl enable postgresql.service
systemctl start postgresql.service
systemctl restart php-fpm.service
su --shell=/bin/bash "${USERNAME}" --command="psql --command=\"ALTER ROLE ${USERNAME} WITH ENCRYPTED PASSWORD '${USERNAME}'\""
