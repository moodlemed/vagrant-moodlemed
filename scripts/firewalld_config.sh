#!/bin/bash
#######################################
# Configura o firewalld
# Globals:
#   None
# Arguments:
#   Serviços (variádico)
# Returns:
#   None
#######################################

setsebool -P httpd_can_network_connect=1
setsebool -P httpd_can_network_connect_db=1
setsebool -P httpd_can_network_relay=1
setsebool -P user_tcp_server=1
for service in "$@"; do
  firewall-cmd --permanent --zone=public "--add-service=${service}"
done
firewall-cmd --reload
