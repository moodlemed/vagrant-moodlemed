#!/bin/bash
#######################################
# Instala e configura o PHP, o Zend OPcache e o Xdebug
# Globals:
#   None
# Arguments:
#   Nome do ambiente; "development" (padrão) ou "production"
# Returns:
#   None
#######################################

readonly ENVIRONMENT="${1:-development}"

# https://docs.moodle.org/29/en/PHP#PHP_Extensions_and_libraries
yum --assumeyes install php-gd php-intl php-mbstring php-pecl-zendopcache php-pecl-xdebug php-soap php-xml php-xmlrpc
sed --in-place "s,disable_functions =,disable_functions = pcntl_alarm\,pcntl_fork\,pcntl_waitpid\,pcntl_wait\,pcntl_wifexited\,pcntl_wifstopped\,pcntl_wifsignaled\,pcntl_wexitstatus\,pcntl_wtermsig\,pcntl_wstopsig\,pcntl_signal\,pcntl_signal_dispatch\,pcntl_get_last_error\,pcntl_strerror\,pcntl_sigprocmask\,pcntl_sigwaitinfo\,pcntl_sigtimedwait\,pcntl_exec\,pcntl_getpriority\,pcntl_setpriority\,," /etc/php.ini
sed --in-place "s,expose_php = On,expose_php = Off," /etc/php.ini
sed --in-place "s,;cgi.fix_pathinfo=1,cgi.fix_pathinfo=0," /etc/php.ini
sed --in-place "s,upload_max_filesize = 2M,upload_max_filesize = 5M," /etc/php.ini
sed --in-place "s,allow_url_fopen = On,allow_url_fopen = Off," /etc/php.ini
sed --in-place 's,;date.timezone =,date.timezone = "America/Sao_Paulo",' /etc/php.ini
# https://secure.php.net/manual/en/opcache.configuration.php
# https://docs.moodle.org/29/en/OPcache
sed --in-place "s,;opcache.enable_cli=0,opcache.enable_cli=1," /etc/php.d/opcache.ini
sed --in-place "s,;opcache.use_cwd=1,opcache.use_cwd=1," /etc/php.d/opcache.ini
sed --in-place "s,;opcache.validate_timestamps=1,opcache.validate_timestamps=1," /etc/php.d/opcache.ini
sed --in-place "s,;opcache.revalidate_freq=2,opcache.revalidate_freq=60," /etc/php.d/opcache.ini
sed --in-place "s,;opcache.save_comments=1,opcache.save_comments=1," /etc/php.d/opcache.ini
sed --in-place "s,;opcache.load_comments=1,;opcache.load_comments=0," /etc/php.d/opcache.ini
sed --in-place "s,;opcache.enable_file_override=0,opcache.enable_file_override=0," /etc/php.d/opcache.ini
if [[ "${ENVIRONMENT}" = "development" ]]; then
  sed --in-place "s,error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT,error_reporting = E_ALL," /etc/php.ini
  sed --in-place "s,display_errors = Off,display_errors = On," /etc/php.ini
  sed --in-place "s,display_startup_errors = Off,display_startup_errors = On," /etc/php.ini
  sed --in-place "s,track_errors = Off,track_errors = On," /etc/php.ini
  sed --in-place "s,mysqlnd.collect_memory_statistics = Off,mysqlnd.collect_memory_statistics = On," /etc/php.ini
  sed --in-place "s,session.bug_compat_42 = Off,session.bug_compat_42 = On," /etc/php.ini
  sed --in-place "s,session.bug_compat_warn = Off,session.bug_compat_warn = On," /etc/php.ini
  sed --in-place "s,opcache.enable=1,opcache.enable=0," /etc/php.d/opcache.ini
  sed --in-place "s,opcache.enable_cli=1,opcache.enable_cli=0," /etc/php.d/opcache.ini
  # http://xdebug.org/docs/profiler
  # https://docs.moodle.org/dev/Profiling_PHP#Xdebug
  echo "xdebug.profiler_enable_trigger=1" >> /etc/php.d/xdebug.ini
fi
