#!/bin/bash
#######################################
# Pré-configura o ambiente
# Globals:
#   None
# Arguments:
#   None
# Returns:
#   None
#######################################

yum -y makecache fast
yum -y install deltarpm epel-release policycoreutils-python yum-utils
yum -y update
# https://wiki.centos.org/AdditionalResources/Repositories/CR
yum-config-manager --enable cr
tee -a /etc/security/limits.conf <<END
root hard nofile 8192
root soft nofile 4096
END
