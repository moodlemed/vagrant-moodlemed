#!/bin/bash
#######################################
# Instala e configura o Moodle
# Globals:
#   None
# Arguments:
#   Caminho do diretório "wwwroot"
#   Caminho do diretório "dataroot"
#   Domínio
#   Nome do usuário do PostgreSQL do Moodle
#   Nome do Moodle
#   E-mail do administrador do Moodle
#   Caminho do diretório "pgdata"
#   Porta HTTP; "80" (padrão)
#   Porta HTTPS; "443" (padrão)
#   Nome do usuário do NGINX; "nginx" (padrão)
#   Nome do usuário do PostgreSQL; "postgres" (padrão)
#   Nome do ambiente; "development" (padrão) ou "production"
# Returns:
#   None
#######################################

if [[ "$#" -lt 7 ]]; then
  echo -n "Uso: $0 wwwroot dataroot server_name dbuser shortname adminemail pgdata [http] [https] [nginx] [postgres] [environment]" >&2
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

readonly WWWROOT="$1"
readonly DATAROOT="$2"
readonly SERVER_NAME="$3"
readonly DBUSER="$4"
readonly SHORTNAME="$5"
readonly ADMINEMAIL="$6"
readonly PGDATA="$7"
readonly HTTP="${8:-80}"
readonly HTTPS="${9:-443}"
readonly NGINX="${10:-nginx}"
readonly POSTGRES="${11:-postgres}"
readonly ENVIRONMENT="${12:-development}"
readonly IDXSPC_TABLESPACE_NAME="${DBUSER}_idxspc"
readonly IDXSPC_DIRECTORY="/srv/u01/${IDXSPC_TABLESPACE_NAME}"
readonly TBLSPC_TABLESPACE_NAME="${DBUSER}_tblspc"
readonly TBLSPC_DIRECTORY="/srv/u02/${TBLSPC_TABLESPACE_NAME}"
readonly DBNAME="${DBUSER}"
readonly DBPASS="${DBUSER}"
readonly FULLNAME="${SHORTNAME}"
readonly SUMMARY="${SHORTNAME}"
readonly ADMINPASS="${DBUSER}"


#######################################
# PHP
#######################################
sed --in-place "s,;open_basedir =,open_basedir = ${WWWROOT}:${DATAROOT}:/usr/share/pear:/usr/share/php:/tmp," /etc/php.ini


#######################################
# nginx
#######################################
mkdir --parents /etc/nginx/ssl
openssl genrsa -out "/etc/nginx/ssl/${SERVER_NAME}.key" 1024
openssl req -out "/etc/nginx/ssl/${SERVER_NAME}.csr" -new -key "/etc/nginx/ssl/${SERVER_NAME}.key" -subj "/C=BR/O=${SHORTNAME}/CN=${SERVER_NAME}"
openssl x509 -in "/etc/nginx/ssl/${SERVER_NAME}.csr" -out "/etc/nginx/ssl/${SERVER_NAME}.crt" -days 365 -signkey "/etc/nginx/ssl/${SERVER_NAME}.key" -req
# http://nginx.org/en/docs/dirindex.html
# http://nginx.org/en/docs/varindex.html
# https://docs.moodle.org/29/en/Nginx#Nginx
# https://docs.moodle.org/29/en/Nginx#XSendfile_aka_X-Accel-Redirect
tee "/etc/nginx/conf.d/${SERVER_NAME}.conf" <<END
server {
    listen ${HTTP};
    listen ${HTTPS} ssl;
    server_name ${SERVER_NAME};
    root "${WWWROOT}";
    index index.html index.htm index.php;
    charset utf-8;
    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }
    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt { access_log off; log_not_found off; }
    error_log /var/log/nginx/${SERVER_NAME}-error.log;
    client_max_body_size 5m;
    location ~ /\.ht {
        deny all;
    }
    ssl_certificate /etc/nginx/ssl/${SERVER_NAME}.crt;
    ssl_certificate_key /etc/nginx/ssl/${SERVER_NAME}.key;
    location /dataroot/ {
        internal;
        alias ${DATAROOT}/;
    }
    location ~ [^/]\.php(/|\$) {
        fastcgi_split_path_info ^(.+\.php)(/.+)\$;
        fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param PATH_INFO \$fastcgi_path_info;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_connect_timeout 300;
        fastcgi_read_timeout 300;
        fastcgi_send_timeout 300;
END
if [[ "${ENVIRONMENT}" = "development" ]]; then
  tee --append "/etc/nginx/conf.d/${SERVER_NAME}.conf" <<END
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_busy_buffers_size 16k;
        fastcgi_temp_file_write_size 16k;
    }
    access_log off;
}
END
else
  tee --append "/etc/nginx/conf.d/${SERVER_NAME}.conf" <<END
        fastcgi_buffer_size 256k;
        fastcgi_buffers 4 256k;
        fastcgi_busy_buffers_size 256k;
        fastcgi_temp_file_write_size 256k;
    }
    access_log /var/log/nginx/${SERVER_NAME}-access.log;
}
END
fi
systemctl restart nginx.service
systemctl restart php-fpm.service


#######################################
# PostgreSQL
#######################################
# http://www.postgresql.org/docs/9.2/static/auth-pg-hba-conf.html
tee --append "${PGSQL_PGDATA}/pg_hba.conf" <<END
local ${DBNAME} ${DBUSER} ident map=${ENVIRONMENT}
END
# http://www.postgresql.org/docs/9.2/static/auth-username-maps.html
tee --append "${PGSQL_PGDATA}/pg_ident.conf" <<END
${ENVIRONMENT} ${POSTGRES} ${DBUSER}
${ENVIRONMENT} ${DBUSER} ${DBUSER}
END
systemctl restart postgresql.service


#######################################
# Moodle
#######################################
yum --assumeyes install aspell ghostscript graphviz

# https://docs.moodle.org/29/en/Installing_Moodle#Download_and_copy_files_into_place
git clone --branch MOODLE_29_STABLE --depth=1 git://git.moodle.org/moodle.git "${WWWROOT}"
mkdir --parents "${WWWROOT}"
tee "${WWWROOT}/robots.txt" <<END
User-Agent: *
Disallow: /
END
chmod --recursive 0755 "${WWWROOT}"
find "${WWWROOT}" -type f -exec chmod 0644 '{}' \;
ln --symbolic /usr/bin/gs "${WWWROOT}/gs"
ln --symbolic /usr/bin/du "${WWWROOT}/du"
ln --symbolic /usr/bin/aspell "${WWWROOT}/aspell"
ln --symbolic /usr/bin/dot "${WWWROOT}/dot"
chown --recursive "${NGINX}:${NGINX}" "${WWWROOT}"

# https://docs.moodle.org/29/en/Installing_Moodle#Create_an_empty_database
# http://www.postgresql.org/docs/9.2/static/sql-createtablespace.html
# http://www.postgresql.org/docs/9.2/static/app-createuser.html
# http://www.postgresql.org/docs/9.2/static/app-createdb.html
# /usr/share/doc/postgresql-9.2.10/README.rpm-dist
mkdir --parents "${IDXSPC_DIRECTORY}" "${TBLSPC_DIRECTORY}"
chown "${POSTGRES}:${POSTGRES}" --recursive "${IDXSPC_DIRECTORY}" "${TBLSPC_DIRECTORY}"
semanage fcontext --add --type postgresql_db_t "${IDXSPC_DIRECTORY}(/.*)?"
restorecon -R "${IDXSPC_DIRECTORY}"
semanage fcontext --add --type postgresql_db_t "${TBLSPC_DIRECTORY}(/.*)?"
restorecon -R "${TBLSPC_DIRECTORY}"
su --shell=/bin/bash "${POSTGRES}" --command="psql --command=\"CREATE TABLESPACE ${IDXSPC_TABLESPACE_NAME} LOCATION '${IDXSPC_DIRECTORY}'\""
su --shell=/bin/bash "${POSTGRES}" --command="psql --command=\"CREATE TABLESPACE ${TBLSPC_TABLESPACE_NAME} LOCATION '${TBLSPC_DIRECTORY}'\""
su --shell=/bin/bash "${POSTGRES}" --command="createuser --no-superuser --no-createdb --no-createrole --no-inherit --connection-limit=30 --encrypted --no-password ${DBUSER}"
su --shell=/bin/bash "${POSTGRES}" --command="psql --command=\"ALTER ROLE ${DBUSER} WITH ENCRYPTED PASSWORD '${DBPASS}'\""
su --shell=/bin/bash "${POSTGRES}" --command="createdb --owner=${DBUSER} --encoding=UTF8 --tablespace=${TBLSPC_TABLESPACE_NAME} ${DBNAME}"

# https://docs.moodle.org/29/en/Installing_Moodle#Create_the_.28moodledata.29_data_directory
# https://docs.moodle.org/29/en/Installing_Moodle#Securing_moodledata_in_a_web_directory
mkdir --parents "${DATAROOT}"
chown --recursive "${NGINX}:${NGINX}" "${DATAROOT}"
tee "${DATAROOT}/.htaccess" <<END
order deny,allow
deny from all
END
chmod --recursive 0777 "${DATAROOT}"

# https://docs.moodle.org/29/en/Installing_Moodle#Command_line_installer
cd "${WWWROOT}" || exit
chmod a+w "${WWWROOT}"
su --shell=/bin/bash "${NGINX}" --command="/usr/bin/php ${WWWROOT}/admin/cli/install.php --lang=\"pt_br\" --wwwroot=\"http://${SERVER_NAME}\" --dataroot=\"${DATAROOT}\" --dbtype=\"pgsql\" --dbname=\"${DBNAME}\" --dbuser=\"${DBUSER}\" --dbpass=\"${DBPASS}\" --fullname=\"${FULLNAME}\" --shortname=\"${SHORTNAME}\" --summary=\"${SUMMARY}\" --adminpass=\"${ADMINPASS}\" --adminemail=\"${ADMINEMAIL}\" --non-interactive --agree-license"
chmod 0644 "${WWWROOT}/config.php"
chmod 0755 "${WWWROOT}"

mv "${WWWROOT}/config.php" "${WWWROOT}/config.php.orig"
head --lines=-5 "${WWWROOT}/config.php.orig" > "${WWWROOT}/config.php"
# https://docs.moodle.org/29/en/Debugging#In_config.php
if [[ "${ENVIRONMENT}" = "development" ]]; then
  tee --append "${WWWROOT}/config.php"  <<END
@error_reporting(E_ALL | E_STRICT);
@ini_set('display_errors', '1');
\$CFG->debug = (E_ALL | E_STRICT);
\$CFG->debugdisplay = 1;
END
fi
# https://docs.moodle.org/29/en/Nginx#XSendfile_aka_X-Accel-Redirect
tee --append "${WWWROOT}/config.php" <<END
\$CFG->xsendfile = 'X-Accel-Redirect';
\$CFG->xsendfilealiases = array(
    '/dataroot/' => \$CFG->dataroot
);
\$CFG->pathtogs = dirname(__FILE__) . '/gs';
\$CFG->pathtodu = dirname(__FILE__) . '/du';
\$CFG->aspellpath = dirname(__FILE__) . '/aspell';
\$CFG->pathtodot = dirname(__FILE__) . '/dot';
END
tail --lines=5 "${WWWROOT}/config.php.orig" >> "${WWWROOT}/config.php"
rm "${WWWROOT}/config.php.orig"

# https://docs.moodle.org/29/en/Cron
crontab -u "${NGINX}" -l > /tmp/crontab
echo "*/30 * * * * /usr/bin/php ${WWWROOT}/admin/cli/cron.php > /dev/null" >> /tmp/crontab
crontab -u "${NGINX}" /tmp/crontab
rm /tmp/crontab
