#!/bin/bash
#######################################
# Cria o grupo "nome do grupo" e os usuários contidos em "users.csv"
# Globals:
#   None
# Arguments:
#   Caminho do diretório "wwwroot"
#   Caminho do diretório "dataroot"
#   Nome do grupo; "development" (padrão) ou "production"
#   Nome do usuário administrador do PostgreSQL; "postgres" (padrão)
# Returns:
#   None
#######################################

if [[ "$#" -lt 2 ]]; then
  echo "Usage: $0 wwwroot dataroot [group] [postgres]" >&2
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

readonly GROUP="${3:-development}"
readonly POSTGRES="${4:-postgres}"

cwd=.
if [[ "$(dmidecode -s system-product-name)" = "VirtualBox" ]]; then
  cwd=/vagrant
fi

#######################################
# Testa a existência de um "role" no banco de dados
# Globals:
#   None
# Arguments:
#   Nome do "role" no banco de dados; "development" (padrão) ou "production"
#   Nome do usuário administrador do PostgreSQL; "postgres" (padrão)
# Returns:
#   "" para não existe
#######################################
role_exists() {
  local rolname="${1:-${GROUP}}"
  local postgres="${2:-${POSTGRES}}"
  su -s /bin/bash "${postgres}" -c "psql -Atc \
    \"SELECT rolname FROM pg_roles WHERE rolname = '${rolname}'\""
}

#######################################
# Cria os grupos "ambiente" no Linux e no PostgreSQL
# Globals:
#   None
# Arguments:
#   Nome do grupo; "development" (padrão) ou "production"
#   Nome do usuário administrador do PostgreSQL; "postgres" (padrão)
# Returns:
#   None
#######################################
group_add() {
  local group="${1:-${GROUP}}"
  local postgres="${2:-${POSTGRES}}"
  if [[ -z "$(getent group "${group}")" ]]; then
    groupadd "${group}"
  fi
  if [[ -z "$(role_exists "${group}" "${postgres}")" ]]; then
    su -s /bin/bash "${postgres}" -c "createuser -SdRILEwe ${group}"
  fi
}

#######################################
# Cria os usuários no Linux e no PostgreSQL
# Globals:
#   None
# Arguments:
#   Nome do usuário
#   Senha inicial do usuário
#   Nome do grupo; "development" (padrão) ou "production"
#   Nome do usuário administrador do PostgreSQL; "postgres" (padrão)
# Returns:
#   None
#######################################
user_add() {
  local group="${3:-${GROUP}}"
  local postgres="${4:-${POSTGRES}}"
  if [[ -z "$(getent passwd "$1")" ]]; then
    useradd -G "${group}" "$1"
    echo "$1:$2" | chpasswd
  fi
  if [[ -z "$(role_exists "$1" "${postgres}")" ]]; then
    su -s /bin/bash "${postgres}" -c "createuser -SdRilEwe $1"
    su -s /bin/bash "${postgres}" -c "psql -Atc \"GRANT ${group} TO $1\""
    su -s /bin/bash "${postgres}" -c "psql -Atc \
      \"ALTER ROLE $1 WITH ENCRYPTED PASSWORD '$2'\""
  fi
}

#######################################
# Altera as permissões de um diretório
# Globals:
#   None
# Arguments:
#   Caminhos dos diretórios (variádico)
# Returns:
#   None
#######################################
auto_chmod() {
  for path in "$@"; do
    find "${path}" -type d -exec chmod 0777 '{}' \;
    find "${path}" -type f -exec chmod 0666 '{}' \;
  done
}

#######################################
# Cria as chaves SSH, as comprime e adiciona a chave SSH pública ao Git
# Globals:
#   None
# Arguments:
#   Nome do usuário
# Returns:
#   None
#######################################
auto_ssh_keygen() {
  mkdir -p "/home/$1/.ssh"
  ssh-keygen -b 4096 -t rsa -N "" -C "$1" -f "/home/$1/.ssh/id_rsa"
  tar -czf "/home/$1/ssh_keys.tar.gz" -C "/home/$1/.ssh" id_rsa id_rsa.pub
  if [[ -f /home/git/.ssh/authorized_keys ]]; then
    cat "/home/$1/.ssh/id_rsa.pub" >> /home/git/.ssh/authorized_keys
  fi
}

#######################################
# Cria links simbólicos para diretórios no diretório de um usuário
# Globals:
#   None
# Arguments:
#   Nome do usuário
#   Caminhos dos diretórios (variádico)
#   Nomes dos links simbólicos (variádico)
# Returns:
#   None
#######################################
auto_ln() {
  local username="$1"
  shift
  while [[ -n "$@" ]]; do
    su "${username}" -c "ln -s $1 /home/${username}/$2"
    shift 2
  done
}

group_add "${GROUP}"
auto_chmod "$1" "$2"

while IFS=, read -r login password; do
  user_add "${login}" "${password}" "${GROUP}" "${POSTGRES}"
  auto_ln "${login}" "$1" "wwwroot" "$2" "dataroot"
  auto_ssh_keygen "${login}"
done < "${cwd}/users.csv"
