#!/bin/bash
#######################################
# Move os índices de um banco de dados para um espaço de tabelas
# Globals:
#   None
# Arguments:
#   Nome do banco de dados
#   Nome do espaço de tabelas
#   Nome do usuário do PostgreSQL; "postgres" (padrão)
# Returns:
#   None
#######################################

if [[ "$#" -lt 2 ]]; then
  echo "Usage: $0 dbname tablespace_name [user]" >&2
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

readonly USER="${3:-postgres}"

if [[ ! "$(su "${USER}" -c "psql -Atc \"\db $2\"")" ]]; then
  echo "Tablespace $2 not found."
  # shellcheck disable=SC2086
  exit ${E_DID_NOTHING}
fi

_command="SELECT 'ALTER INDEX '||indexname||' SET TABLESPACE $2;'\
  FROM pg_catalog.pg_indexes\
  WHERE schemaname = 'public';"
su "${USER}" -c "psql -Atd $1 -c \"${_command}\" | psql -d $1"
