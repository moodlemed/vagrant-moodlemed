vagrant-moodlemed
=================

Vagrantfile para criação de máquinas virtuais com o Moodle 2.9.

O provisionamento começa em [`scripts/environment_config.sh`](scripts/environment_config.sh).

Instalação
----------

### Instalando o VirtualBox

    sudo add-apt-repository "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release --codename --short) contrib"
    wget --output-document=- --quiet https://www.virtualbox.org/download/oracle_vbox_2016.asc | sudo apt-key add -
    wget --output-document=- --quiet https://www.virtualbox.org/download/oracle_vbox.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install --assume-yes virtualbox-5.1

### Instalando o Vagrant

    wget --output-document=/tmp/vagrant_1.8.5_x86_64.deb https://releases.hashicorp.com/vagrant/1.8.5/vagrant_1.8.5_x86_64.deb
    sudo dpkg --install /tmp/vagrant_1.8.5_x86_64.deb
    sudo apt-get install --fix-broken --assume-yes
    vagrant plugin install vagrant-vbguest

Uso
---

### Criando uma máquina virtual de desenvolvimento

    make clean
    make development
    make

### Criando uma máquina virtual de produção

    make clean
    make production
    make
