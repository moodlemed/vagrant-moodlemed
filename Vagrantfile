require_relative "environment"

Vagrant.configure(2) do |config|
  config.ssh.username = "vagrant"
  config.ssh.password = "vagrant"
  config.vm.box = "bento/centos-7.2"
  config.vm.hostname = SETTINGS[:hostname] ||= "centos72"
  config.vm.network "forwarded_port",
    guest: 80,
    host: 8008,
    auto_correct: true
  config.vm.network "forwarded_port",
    guest: 443,
    host: 8443,
    auto_correct: true
  config.vm.network "forwarded_port",
    guest: 5432,
    host: 15432,
    auto_correct: true
  config.vm.network :private_network, ip: SETTINGS[:ip] ||= "192.168.10.10"
  config.vm.provider "virtualbox" do |v|
    v.customize ["modifyvm", :id, "--ostype", "RedHat_64"]
    v.customize ["modifyvm", :id, "--memory", SETTINGS[:memory] ||= 1024]
    v.customize ["modifyvm", :id, "--hwvirtex", "on"]
    v.customize ["modifyvm", :id, "--cpus", SETTINGS[:cpus] ||= 1]
    v.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
    v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    if SETTINGS.key?(:disks)
      SETTINGS[:disks].each_with_index do |d, i|
        unless File.exists?(d[:path])
          v.customize [
            "createhd",
            "--filename", d[:path],
            "--size", (d[:size] ||= 1024) * 1024,
            "--format", (d[:format] ||= "VMDK")
          ]
        end
        v.customize [
          "storageattach", :id,
          "--storagectl", "SATA Controller",
          "--port", i + 1,
          "--device", 0,
          "--type", "hdd",
          "--medium", d[:path]
        ]
      end
    end
  end
  config.vm.post_up_message = <<-END
The virtual #{ENVIRONMENT} server is online at #{SETTINGS[:scripts][1][:args][0]}.
Do not forget to run 'hosts.bat' or 'hosts.sh'!
END
  if SETTINGS.key?(:scripts)
    SETTINGS[:scripts].each do |s|
      if File.exists?(s[:path])
        config.vm.provision "shell", path: s[:path], args: s[:args] ||= []
      end
    end
  end
end
